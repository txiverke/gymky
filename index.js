/**
 * Initialize the environment
 * @type {string|*}
 */
process.env.NODE_ENV = process.env.NODE_ENV || 'production';

/**
 * Require dependencies
 */
var mongoose = require('./config/mongoose');
var express = require('./config/express');
var passport = require('./config/passport');

/**
 * Initialize the Gymky.me App
 */
var db = mongoose();
var app = express();
var passport = passport();
var port = process.env.PORT || 3000;

module.exports = app;

app.listen(port, function() {
    console.log('%d listening on %d', process.pid, port);
});