/**
 * Created by xavi on 10/10/15.
 */
/**
 * Gulp Dependencies
 * @type {Gulp|exports}
 */
var gulp    = require('gulp');
var clean   = require('gulp-clean');
var buffer  = require('vinyl-buffer');
var source  = require('vinyl-source-stream');
var concat  = require('gulp-concat');
var uglify  = require('gulp-uglify');
var sass    = require('gulp-sass');
var minify  = require('gulp-minify-css');
var assign  = require('lodash.assign');
var jsonminify = require('gulp-jsonminify');

gulp.task('default', [
    'delete-app',
    'generate-js',
    'generate-css',
    'generate-translations'
]);

gulp.task('delete-app',function(){
    return gulp.src([
        './public/*.js',
        './public/css/app.css'
    ])
        .pipe(clean());
});

gulp.task('generate-css', function () {
    return gulp.src('./public/scss/app.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('app.css'))
        .pipe(minify())
        .pipe(gulp.dest('./public/css'));
});

gulp.task('generate-js', function () {

    console.log('Compress-JS init');

    return gulp.src([
        './node_modules/angular/angular.js',
        './node_modules/angular-ui-router/release/angular-ui-router.js',
        './node_modules/angular-resource/angular-resource.js',
        './node_modules/angular-animate/angular-animate.min.js',
        './node_modules/angular-aria/angular-aria.min.js',
        './node_modules/angular-file-upload/dist/angular-file-upload.js',
        './node_modules/jquery/dist/jquery.js',
        './node_modules/ng-file-upload/dist/ng-file-upload-shim.js',
        './node_modules/ng-file-upload/dist/ng-file-upload.js',
        './node_modules/angular-cookies/angular-cookies.js',
        './node_modules/angular-confirm/angular-confirm.js',
        './node_modules/angular-translate/dist/angular-translate.js',
        './node_modules/angular-translate-handler-log/angular-translate-handler-log.js',
        './node_modules/angular-translate-loader-static-files/angular-translate-loader-static-files.js',
        './node_modules/angular-dynamic-locale/dist/tmhDynamicLocale.js',
        './node_modules/angular-i18n/angular-locale_en-us.js',
        './node_modules/angular-i18n/angular-locale_es.js',

        './public/app/locale/**/*.js',
        './public/app/login/**/*.js',
        './public/app/menu/**/*.js',
        './public/app/users/**/*.js',
        './public/app/activities/**/*.js',
        './public/app/profile/**/*.js',
        './public/app/games/**/*.js',
        './public/app/footer/**/*.js',
        './public/app/shared/**/*.js',
        './public/app/app.js',
        './public/app/_config/app.config.js',
        './public/app/_config/app.run.js',
        './public/app/_config/routes/*.js'
    ])
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public/'));
});

gulp.task('generate-translations', function() {

    console.log('Minify Translations');

    return gulp.src('./public/app/locale/translations/*.json')
        .pipe(jsonminify())
        .pipe(gulp.dest('./public/translations/'))

});

