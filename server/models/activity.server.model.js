/**
 * Gymky.me
 * Created by xavi on 12/10/15.
 */

/**
 * Model Dependencies
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Activity Model
 * @type {*|Schema}
 */
var ActivitySchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    background: {
        type: String
    },
    title: {
        type: String,
        default: '',
        trim: true,
        required: 'Title cannot be blank'
    },
    task: [
        {
            id: {
                type: Number,
                index: true
            },
            user: [
                {
                    userId: String,
                    responses : [
                        {
                            id: Number,
                            state: {
                                type: String,
                                enum: ['Active', 'Done', 'Fail']
                            }
                        }
                    ]
                }
            ],
            kind_of: {
                type: String,
                enum: ['Clue', 'Question', 'Multiple', 'Picture', 'Position']
            },
            title: {
                type: String,
                trim: true
            },
            response:{
                type: String
            },
            responses: [
                {
                    value: {
                        type: String
                    },
                    state: {
                        type: Boolean
                    }
                }
            ]

        }
    ],
    type: {
        type: String,
        enum: ['Public', 'Private']
    },
    state: {
        type: String,
        enum: ['Open', 'Active', 'Closed']
    },
    team: [
        {
            title   : String,
            leader  : String,
            email   : String
        }
    ],
    activity_date: {
       type: Date
    },
    rating: {
        type: Number
    },
    creator: {
        type: Schema.ObjectId,
        ref: 'User'
    }
});

mongoose.model('Activity', ActivitySchema);