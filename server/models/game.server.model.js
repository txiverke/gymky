/**
 * Gymky.me
 * Created by xavi on 01/01/16.
 */

'use strict';

/**
 * Model Dependencies
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * Game Model
 * @type {*|Schema}
 */
var GameSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    activityId: {
        type: String
    },
    teamId: {
        type: String
    },
    teamName: {
        type: String
    },
    email : {
        type : String
    },
    leader: {
        type: String
    },
    gameTitle: {
        type: String
    },
    task: [
        {
            id: {
                type: Number,
                index: true
            },
            kind_of: String,
            title: String,
            state: {
                type: String,
                enum: ['Active', 'Done', 'Fail']
            },
            response: String
        }
    ],
    completed: {
        type: Date
    }

});

mongoose.model('Game', GameSchema);