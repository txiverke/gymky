/**
 * Gymky.me
 * Created by xavi on 13/10/15.
 */

'use strict';

var users = require('../../server/controllers/users.server.controller');
var activities = require('../../server/controllers/activities.server.controller');

module.exports = function( app ) {

    app.route('/api/activities')
        .get(activities.list)
        .post(users.requiresLogin, activities.create);

    app.route('/api/activities/:activityId')
        .get(activities.read)
        .put(users.requiresLogin, activities.update)
        .delete(users.requiresLogin, activities.delete);

    app.route('/api/activities/:activityId/teams')
        .get(activities.teamByID);

    app.route('/api/activities/login/:activityId/:leaderEmail')
        .get(activities.activityByLeaderEmail);

    app.route('/api/activities/email/:activityId')
        .get(activities.sendMail);

    app.route('/api/activities/task/:activityId/:task')
        .get(activities.taskByID);

    app.route('/api/activities/question/:activityId/:task/:response/:userId')
        .get(activities.activityQuestionResponse);

    app.route('/api/activities/:activityId/background')
        .put(activities.updateBackground);

    app.route('/api/activities/background/:activityId/:task/:userId')
        .put(activities.updateTaskBackground);

    app.param('activityId', activities.activityByID);

};
