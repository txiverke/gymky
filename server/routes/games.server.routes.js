/**
 * Gymky.me
 * Created by xavi on 01/01/16.
 */

'use strict';

var games = require('../../server/controllers/games.server.controller');

module.exports = function( app ) {

    app.route('/api/games')
        .get(games.list)
        .post(games.create);

    app.route('/api/games/:gameId')
        .get(games.read)
        .put(games.update);

    app.route('/api/games/:gameId/taskId')
        .get(games.getTaskId);

    app.route('/api/games/:gameId/results')
        .get(games.getResults);

    app.route('/api/games/:activityId/:email')
        .get(games.existingOldGame);

    app.param('gameId', games.gameByID);



};
