/**
 * Gymky.me
 * Created by xavi on 11/10/15.
 */

var users = require('../../server/controllers/users.server.controller')
  , passport = require('passport');

module.exports = function(app) {

    app.route('/signup')
        .get(users.renderSignup)
        .post(users.signup);

    app.route('/signin')
        .get(users.renderSignin)
        .post(passport.authenticate('local', {
            successRedirect: '/',
            failureRedirect: '/signin',
            failureFlash: true
        }));

    app.get('/signout', users.signout);

    app.get('/oauth/facebook', passport.authenticate('facebook', {
        failureRedirect: '/signin'
    }));
    
    app.get('/oauth/facebook/callback', passport.authenticate('facebook', {
        failureRedirect: '/signin',
        successRedirect: '/'
    }));

    app.route('/api/users')
        .post(users.create)
        .get(users.list);

    app.route('/api/users/:userId')
        .get(users.read)
        .put(users.update)
        .delete(users.delete);

    app.route('/api/users/:userId/avatar')
        .put(users.updateAvatar);

    app.param('userId', users.userByID);

};