/**
 * Gymky.me
 * Created by xavi on 12/10/15.
 */

'use strict';

var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var mg = require('nodemailer-mailgun-transport');
var Activity = mongoose.model('Activity');
var uuid = require('node-uuid');
var multiparty = require('multiparty');
var fs = require('fs');

/**
 * Create one Activity
 * @param req
 * @param res
 */
exports.create = function(req, res) {

    var activity = new Activity(req.body);
    activity.creator = req.user;

    activity.save(function(err) {

        if (err) {
           return res.status(400).send({
               message: getErrorsMessage(err)
           });
       } else {
            res.json( activity )
        }

    });

};

/**
 * Get all the Activities
 * @param req
 * @param res
 */
exports.list = function(req, res) {

    Activity
        .find({
            creator : req.user.id
        })
        .sort('-created')
        .populate('creator', 'firstName lastName fullName')
        .exec(
            function(err, activities) {
                if (err) {
                    return res.status(400).send({
                        message: getErrorsMessage(err)
                    });
                } else {
                    res.json({ activities: activities});
                }
        }
    );

};

/**
 * Get One Activity
 * @param req
 * @param res
 * @param next
 * @param id
 */
exports.activityByID = function(req, res, next, id) {

    Activity
        .findById(id)
        .populate('creator', 'firstName lastName fullName')
        .exec(
        function(err, activity){

            if (err || !activity) {
                return res.status(404).send({
                    message: getErrorsMessage(err)
                });
            }

            req.activity = activity;
            next();
        }
    );

};

/**
 * Get one Activity Response
 * @param req
 * @param res
 */
exports.read = function(req, res) {

    res.json(req.activity);

};

/**
 * Update one Activity
 * @param req
 * @param res
 */
exports.update = function(req, res) {

    var activity = req.activity;

    activity.title  = req.body.title;
    activity.state  = req.body.state;
    activity.rating = req.body.rating;
    activity.team   = req.body.team;
    activity.task   = req.body.task;
    activity.activity_date = req.body.activity_date;
    activity.background = req.body.background;

    activity.save(function(err) {
        if (err) {
            return res.status(404).send({
                message: getErrorsMessage(err)
            });
        } else {
            res.json(activity);
        }
    });

};

/**
 * Get one activity and then the response is just public data
 * @param req
 * @param res
 */
exports.teamByID = function (req, res) {

    var activity = req.activity;
    var response;

    if (!activity) {

        return res.status(404).send();

    }

    response = {
        _id         : req.activity._id,
        title       : req.activity.title,
        date        : req.activity.activity_date,
        background  : req.activity.background,
        tasks       : req.activity.task.length
    };

    res.json(response);

};


/**
 * Check if the user is a team leader and is able to acces to the Game
 * @param req
 * @param res
 */
exports.activityByLeaderEmail = function( req, res ) {

    var id = req.params.activityId;
    var email = req.params.leaderEmail;

    Activity
        .findById( id )
        .exec(function(err, activity) {

            if (err || !activity) {
                return res.status(404).send({
                    message: getErrorsMessage(err)
                });
            } else {

                var response = { result : false };
                var team = activity.team;
                var match = false;
                var i = 0;

                console.log('team')

                for (i; i < team.length; i++) {

                    if ( team[i].email === email ) {
                        console.log('entra')
                        response = {
                            team    : team[i],
                            result  : true
                        };
                        match = true;
                        break;
                    }
                }

                if ( match ) {
                    res.json(response);
                }

                if ( !match && i === team.length ){
                    res.json(response);
                }


            }
        }
    );

};

exports.activityByEmail = function( req, res, next, id ){


    Activity
        .findById( id )
        .exec(function( err, activity ) {
            if ( err ) {
                return res.status(400).send({
                    message: getErrorsMessage(err)
                });
            } else {
                req.activity = activity;
                next();

            }
        }
    );

};

/**
 * Send Mail to the Team leaders
 * @param req
 * @param res
 */
exports.sendMail = function(req, res){

    var team = req.activity.team;
    var activityId = req.activity._id;
    var gymkyMail = 'info@gymky.me';
    var gymkyMessage = 'You have been added as a leader of the ';
    var gymkyMessage1 = ' in a Treasure Hunt in Gymky.me. To access to your game just click the following link ';
    var gymkyLink = "http://gymky.me/#!/treasure-hunt/" + activityId + "/login";
    var sent = false;

    var auth = {
        auth: {
            api_key: 'key-e1aa39d86890bf56236d27724a8668fb',
            domain: 'gymky.me'
        }
    };

    var nodemailerMailgun = nodemailer.createTransport(mg(auth));

    for ( var i = 0; i < team.length; i++ ) {
        nodemailerMailgun.sendMail({
            from : gymkyMail,
            to : team[i].email,
            subject : 'Hello ' + team[i].leader,
            text : gymkyMessage + team[i].title + gymkyMessage1 + gymkyLink
        }, function(error, response) {
            if (error) {
                console.log(error);
            } else {
                console.log('Message sent');
            }
        });
    }

    res.json(req.activity);

};

/**
 * Get the tasks to the Game
 * @param req
 * @param res
 * @param id
 * @param next
 */
exports.taskByID = function (req, res) {

    var id      = req.params.activityId;
    var taskId  = req.params.task;

    Activity
        .findById(id)
        .exec(function (err, activity) {

            if (err) {
                return res.status(404).send({
                    message: getErrorsMessage(err)
                });
            } else {

                var task = activity.task;
                var response = {};

                if ( task.length > taskId) {

                    var kind_of = task[taskId].kind_of;

                    if (kind_of === 'Multiple') {

                        response = {
                            id: taskId,
                            total: task.length,
                            kind_of: kind_of,
                            title: task[taskId].title,
                            responses: [
                                {value: task[taskId].responses[0].value},
                                {value: task[taskId].responses[1].value},
                                {value: task[taskId].responses[2].value}
                            ]
                        };

                        res.json(response);

                    } else if (kind_of === 'Question' ||
                        kind_of === 'Picture' ||
                        kind_of === 'Position' ||
                        kind_of === 'Clue') {

                        response = {
                            id: taskId,
                            total: task.length,
                            kind_of: kind_of,
                            title: task[taskId].title
                        };

                        res.json(response);

                    }
                } else {

                     res.json({result: true});

                }
            }
        }
    );

};

exports.updateTaskBackground = function(req, res) {

    var activityId = req.params.activityId;
    var taskId = req.params.task;
    var userId = req.params.userId;
    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files) {

        var file = files.file[0];
        var contentType = file.headers['content-type'];
        var tmpPath = file.path;
        var extIndex = tmpPath.lastIndexOf('.');
        var extension = (extIndex < 0) ? '' : tmpPath.substr(extIndex);
        var activity = req.activity;
        var currentDate = Date.now();
        var fileName = taskId + '_' + activity._id + '_' + currentDate + extension;
        var destPath = 'public/imgs/activities/tasks/' + fileName;

        if (contentType !== 'image/png' && contentType !== 'image/jpeg') {
            fs.unlink(tmpPath);
            return res.status(400).send('Unsupported file type.');
        }

        fs.rename(tmpPath, destPath, function(err) {

            if (err) return res.status(400).send('Image is not saved:');

            Activity
                .findById(activityId)
                .exec(function (err, activity) {

                    if (err) return next(err);

                    var user = {};
                    var answers = [];
                    var game = [];
                    var result = true;

                    game.push({
                        id      : taskId,
                        kind_of : 'Picture',
                        title   :  activity.task[taskId].title,
                        state   : 'Done',
                        response: fileName
                    });

                    answers.push({
                        id      : taskId,
                        state   : 'Done'
                    });

                    user.responses = answers;
                    user.userId = userId;
                    activity.task[taskId].user.push(user);


                    Activity.findByIdAndUpdate(activity._id, activity, function(err) {

                        if(err) return next(err);

                        res.json({
                            response    : result,
                            taskId      : taskId,
                            responses   : game
                        });

                    })

                })

        });
    });

};


/**
 * Check the tasks responses
 * @param req
 * @param res
 * @param next
 * @param leaderEmail
 */
exports.activityQuestionResponse = function(req, res) {

    var activity = req.activity;
    var taskId = req.params.task;
    var response = req.params.response;
    var userId = req.params.userId;
    var result = false;
    var state = 'Fail';
    var user = {};
    var answers = [];
    var game = [];
    var task;
    var type_of;


    if (!activity) {
        return res.status(404).send();
    }

    task = activity.task;
    type_of = task[taskId].kind_of;

    switch (type_of) {

        case 'Multiple':
            var responses = task[taskId].responses;
            for ( var i = 0; i < 3; i++) {

                if ( responses[i].value === response &&
                    responses[i].state === true) {
                    result = true;
                    state = 'Done';
                    break;
                }
            }
            break;

        case 'Question':
            var oneResponse = task[taskId].response;
            if ( oneResponse === response.toLowerCase() ) {
                result = true;
                state = 'Done';
            }
            break;

        case 'Clue':
            result = true;
            state = 'Done';
            break;
    }

    game.push({
        id      : taskId,
        kind_of : task[taskId].kind_of,
        title   : task[taskId].title,
        state   : state,
        response: response
    });

    answers.push({
        id      : taskId,
        state   : state
    });

    user.responses = answers;
    user.userId = userId;
    activity.task[taskId].user.push(user);

    Activity.findByIdAndUpdate(activity._id, activity, function(err) {

        if(err) return next(err);

        res.json({
            response    : result,
            taskId      : taskId,
            responses   : game
        });

    })

};

/**
 * Delete Activity
 * @param req
 * @param res
 */
exports.delete = function(req, res) {

    var activity = req.activity;

    activity.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: getErrorsMessage(err)
            });
        } else {
            res.json(activity);
        }
    });

};

exports.hasAuthorization = function(req, res, next) {

    var activityCreator = req.activity.creator;
    var user = req.user.id;

    if(activityCreator !== user) {
        return res.status(403).send({
            message: 'User is not authorized'
        })
    }

    next();

};



exports.updateBackground = function(req, res) {

    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files) {

        var file = files.file[0];
        var contentType = file.headers['content-type'];
        var tmpPath = file.path;
        var extIndex = tmpPath.lastIndexOf('.');
        var extension = (extIndex < 0) ? '' : tmpPath.substr(extIndex);
        var activity = req.activity;
        var currentDate = Date.now();
        var fileName = activity._id + '_' + currentDate + extension;
        var destPath = 'public/imgs/activities/' + fileName;

        if (contentType !== 'image/png' && contentType !== 'image/jpeg') {
            fs.unlink(tmpPath);
            return res.status(400).send('Unsupported file type.');
        }

        fs.rename(tmpPath, destPath, function(err) {

            if (err) return res.status(400).send('Image is not saved:');

            Activity.findOne({ _id: req.activity._id }, function(err, activity) {

                if(err) return next(err);

                var oldBackground= activity.background;

                if( oldBackground != 'people.jpg' ) {
                    fs.unlink('public/imgs/activities/' + oldBackground);
                }

                var newActivity = activity;

                newActivity.background = fileName;

                newActivity.save(function(err) {

                    if (err) {
                        return res
                            .status(400)
                            .send({ message: getErrorsMessage(err) });
                    }

                    res.json(activity)


                });

            })

        });
    });

};

var getErrorsMessage = function(err) {

    if (err.errors) {
        for (var errName in err.errors) {
            if(err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Unknown server error';
    }

};

