/**
 * Gymky.me
 * Created by xavi on 11/10/15.
 */

var User = require('mongoose').model('User');
var passport = require('passport');
var uuid = require('node-uuid');
var multiparty = require('multiparty');
var fs = require('fs');
var reCAPTCHA = require('recaptcha2');
var config = require('../../config/config');


exports.renderSignin = function(req, res) {

    if (!req.user) {
        res.render('signin', {
            title: 'Gymky.me Sign In',
            messages: req.flash('error') || req.flash('info')
        });
    } else {
        return res.redirect('/');
    }
};

exports.renderSignup = function(req, res) {
    if (!req.user) {
        res.render('signup', {
            title: 'Gymky.me Register',
            messages: req.flash('error')
        });
    } else {
        return res.redirect('/');
    }
};

exports.signup = function (req,res) {

    var recaptcha = new reCAPTCHA({
        siteKey     : config.siteKey,
        secretKey   : config.secretKey
    });
    var user;
    var message;

    recaptcha.validateRequest(req)
        .then(function(){

            if (!req.user) {

                user = new User(req.body);

                user.provider = 'local';
                user.avatar = 'default_avatar.png';
                user.type = 'User';

                user.save(function (err) {

                    if (err) {

                        message = getErrorMessage(err);

                        req.flash('error', message);
                        return res.redirect('/signup');
                    }
                    req.login(user, function(err) {

                        if (err) return next(err);

                        return res.redirect('/');

                    });
                });

            } else {
                return res.redirect('/');
            }
        })
        .catch(function(errorCodes){

            var error = {code : 11002};

            if (errorCodes) {

                message = getErrorMessage(error);
                req.flash('error', message);
                return res.redirect('/signup');
            }

        }
    );
};


exports.signout = function(req, res) {
    req.logout();
    res.redirect('/');
};

exports.saveOAuthUserProfile = function(req, profile, done) {
    User.findOne({
        provider: profile.provider,
        providerId: profile.providerId
    }, function(err, user) {
        if (err) {
            return done(err);
        } else {
            if (!user) {
                var possibleUsername = profile.username || ((profile.email) ? profile.email.split('@')[0] : '');

                User.findUniqueUsername(possibleUsername, null, function(availableUsername) {
                    profile.username = availableUsername;

                    user = new User(profile);

                    user.save(function(err) {
                        if (err) {
                            var message = _this.getErrorMessage(err);

                            req.flash('error', message);
                            return res.redirect('/signup');
                        }

                        return done(err, user);
                    });
                });
            } else {
                return done(err, user);
            }
        }
    });
};


exports.create = function(req, res, next) {

    var user = new User(req.body);

    user.save(function(err) {
        if(err) {
            return next(err);
        }else{
            res.json(user);
        }
    })

};

exports.list = function(req, res, next) {

    User.find({}, function(err, users) {
        if (err) {
            return next(err);
        } else {
            res.json(users);
        }
    })

};

exports.read = function(req, res) {
    res.json(req.user);
};

exports.userByID = function(req, res, next, id){

    User.findOne({ _id: id }, function(err, user) {

        if (err) return next(err);

        req.user = user;
        next();

    })

};

exports.update = function(req, res, next) {

    User.findByIdAndUpdate(req.user.id, req.body, function(err, user) {
        if(err){
            return next(err);
        }else{
            res.json(user);
        }
    })

};

exports.updateAvatar = function(req, res) {

    var form = new multiparty.Form();

    form.parse(req, function(err, fields, files) {

        var file = files.file[0];
        var contentType = file.headers['content-type'];
        var tmpPath = file.path;
        var extIndex = tmpPath.lastIndexOf('.');
        var extension = (extIndex < 0) ? '' : tmpPath.substr(extIndex);
        var user = req.user;
        var password = user.password;
        var currentDate = Date.now();
        var fileName = user.username + '_' + currentDate + extension;
        var destPath = 'public/imgs/users/' + fileName;

        if (contentType !== 'image/png' && contentType !== 'image/jpeg') {
            fs.unlink(tmpPath);
            return res.status(400).send('Unsupported file type.');
        }

        fs.rename( tmpPath, destPath, function(err) {

            if (err) return res.status(400).send('Image is not saved:');

            User.findOne({ _id: req.user._id }, function(err, user) {

                if(err) return next(err);

                var oldAvatar = user.avatar;

                if( oldAvatar != 'default_avatar.png' ) {
                    fs.unlink('public/imgs/users/' + oldAvatar);
                }

                var newUser = user;
                newUser.avatar = fileName;
                newUser.password = password;

                if ( newUser.avatar === fileName ) {

                    User.findByIdAndUpdate(req.user.id, newUser, function(err, user) {
                        if(err){
                            return next(err);
                        }else{
                            res.json(newUser);
                        }
                    })

                }

            })

        });
    });

};

exports.delete = function(req, res, next) {

    req.user.remove(function(err) {
        if(err) {
            return next(err);
        } else {
            res.json(req.user);
        }
    })

};

exports.requiresLogin = function(req, res, next) {

    if (!req.isAuthenticated()) {
        return res.status(401).send({
            message: 'User is not logged in'
        });
    }

    next();

};

var getErrorMessage = function(err) {

    var message = '';

    if (err.code) {
        switch (err.code) {
            case 11000:
            case 11001:
                message = 'Username already exists';
                break;
            case 11002:
                message = 'Are you a robot?';
                break;
            default:
                message = 'Something went wrong';
        }
    } else {
        for (var errName in err.errors) {
            if (err.errors[errName].message) message = err.errors[errName].message;
        }
    }

    return message;
};
