/**
 * Gymky.me
 * Created by xavi on 11/10/15.
 */

exports.render = function(req, res){

    if (req.session.lastVisit){
        console.log('Last Visit:', req.session.lastVisit);
    }

    req.session.lastVisit = new Date();

    res.render('index', {
        title: 'Gymky.me',
        user: JSON.stringify(req.user)
    });



};