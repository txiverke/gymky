/**
 * Gymky.me
 * Created by xavi on 02/01/16.
 */

var mongoose = require('mongoose');
var Game = mongoose.model('Game');

exports.list = function () {

};

/**
 * Create a Game Register
 * @param req
 * @param res
 */
exports.create = function(req, res){

    var game = new Game(req.body);

    game.save(function (err) {

        if (err) {
            return res.status(400).send({
                message: getErrorsMessage(err)
            });
        } else {
            res.json(game);
        }

    });

};

exports.read = function(){

};

/**
 * Update Game Responses
 * @param req
 * @param res
 */
exports.update = function(req, res ){

    var game = req.game;

    game.activityId = req.body.activityId;
    game.teamId = req.body.teamId;
    game.teamName = req.body.teamName;
    game.email = req.body.email;
    game.leader = req.body.leader;
    game.gameTitle = req.body.gameTitle;
    game.task.push(req.body.task);
    game.completed = req.body.completed;

    game.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: getErrorsMessage(err)
            });
        } else {
            res.json(game);
        }
    });

};

/**
 * Get one Treasure Hunt Results
 * @param req
 * @param res
 * @param next
 * @param id
 */
exports.gameByID = function(req, res, next, id) {

    Game.findById(id)
        .exec(function (err, game) {

            if (err) {
                return next(err);
            }

            if (!game) {
                return next(new Error('Failed to load activity ' + id));
            }

            req.game = game;
            next();
        }
    );

};

/**
 * Get the taskId of the game
 * @param req
 * @param res
 * @returns {*}
 */
exports.getTaskId = function (req, res) {

    var game = req.game;
    var taskId;

    if (!game) {
        return res.status(404).send();
    }

    taskId = game.task.length;
    res.json({taskId : taskId});

};

/**
 * Get the data of a Completed Game
 * @param req
 * @param res
 * @returns {*}
 */
exports.getResults = function (req, res) {

    var game = req.game;

    if (!game) {
        return res.status(404).send();
    }

    res.json({result: game.task})

};

/**
 * Check if Team leader is Log In
 * @param req
 * @param res
 */
exports.existingOldGame = function (req, res) {

    var activityId = req.params.activityId;
    var email = req.params.email;


    Game.find({
            activityId : activityId,
            email : email
        })
        .exec(function (err, game) {

            if (err) {
                return res.status(400).send({
                    message: getErrorsMessage(err)
                });
            } else {
                if (game.length) {
                    res.json(game[0]);
                } else {
                    return res.status(404).send();
                }
            }
        }
    );

};

/**
 * Handles error Responses
 * @param err
 * @returns {*}
 */
var getErrorsMessage = function(err) {

    if (err.errors) {
        for (var errName in err.errors) {
            if(err.errors[errName].message) {
                return err.errors[errName].message;
            }
        }
    } else {
        return 'Unknown server error';
    }

};



