/**
 * Gymky.me
 * Created by xavi on 31/01/16.
 */

/**
 * Module Dependencies
 */
var app = require('../../index');
var should = require('should');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Activity = mongoose.model('Activity');

/**
 * Global Variables Definition
 */
var user;
var activity;

describe('Activity Model Unit Test:', function(){

    beforeEach(function(done) {

       user = new User({
           firstName    : 'Full',
           lastName     : 'Name',
           displayName  : 'Full Name',
           avatar       : 'avatar.jpg',
           email        : 'test@test.com',
           username     : 'username',
           password     : 'password'
       });

       user.save(function() {

           activity = new Activity({
               title    : 'Activity Title',
               creator  : user
           });

           done();

       });

    });

   describe('Testing the save method', function() {

       it('Should be able to save without problems', function(){

           activity.save(function(err) {

               should.not.exist(err);

           })

       });

       it('Should not be able to save an activity without a title', function(){

           activity.title = '';

           activity.save(function(err) {

               console.log(err)
              should.exist(err);

           });

       });

   });

   afterEach(function (done) {

       Activity.remove(function() {

           User.remove(function() {

               done();

           })

       })

   })

});



