/**
 * Gymky.me
 * Created by xavi on 31/01/16.
 */

/**
 * Module Dependencies
 */
var app = require('../../index');
var request = require('supertest');
var should = require('should');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Activity = mongoose.model('Activity');

/**
 * Global Variables Definition
 */
var user;
var activity;

describe('Activity Model Unit Test:', function(){

    beforeEach(function(done) {

        user = new User({
            firstName    : 'Full',
            lastName     : 'Name',
            displayName  : 'Full Name',
            avatar       : 'avatar.jpg',
            email        : 'test@test.com',
            username     : 'username',
            password     : 'password'
        });

        user.save(function() {

            activity = new Activity({
                title    : 'Activity Title',
                creator  : user
            });

            activity.save(function(err){
                done();
            })

        });

    });

    describe('Testing the GET methods', function() {

        it('Should be able to get the list of activities', function(done){

            request(app).get('/api/activities')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function(err, res) {
                    res.body.should.be.an.Array.and.have.lengthOf(1);
                    res.body[0].should.have.property('title', activity.title);

                    done();
                });
        });

         /* it('Should be able to get the specific activity', function(done) {
            request(app).get('/api/activities/' + activity.id)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function(err, res) {
                    res.body.should.be.an.Object.and.have.property('title', activity.title);

                    done();
                });
        });*/

    });

    afterEach(function(done) {
        Activity.remove().exec();
        User.remove().exec();
        done();
    });
});
