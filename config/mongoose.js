/**
 * Gymky.me
 * Created by xavi on 11/10/15.
 */

var config = require('./config')
  , mongoose = require('mongoose');


module.exports = function() {

    var db = mongoose.connect( config.db );

    require('../server/models/user.server.model');
    require('../server/models/activity.server.model');
    require('../server/models/game.server.model');

    return db;
};