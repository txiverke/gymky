/**
 * Gymky.me
 * Created by xavi on 11/10/15.
 */

var config = require('./config');
var express = require('express');
var morgan = require('morgan');
var compress = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var session = require('cookie-session');
var flash = require('connect-flash');
var passport = require('passport');
var cookieParser = require('cookie-parser');

module.exports = function() {

    var app = express();

    if(process.env.NODE_ENV === 'development'){
        app.use(morgan('dev'));
    }else if (process.env.NODE_ENV === 'production'){
        app.use(compress());
    }

    app.use(bodyParser.urlencoded({
        'extended':'true'
    }));

    app.use(bodyParser.json());
    app.use(bodyParser.json({
        type: 'application/vnd.api+json'
    }));

    app.use(methodOverride());

    app.use(cookieParser(config.sessionSecret));

    app.use(session({
        name: 'gymkySession',
        keys: [config.sessionSecret]
    }));

    app.set('views', './server/views');
    app.set('view engine', 'ejs');

    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session());

    require('../server/routes/index.server.routes.js')(app);
    require('../server/routes/users.server.routes.js')(app);
    require('../server/routes/activities.server.routes.js')(app);
    require('../server/routes/games.server.routes.js')(app);

    app.use(express.static('./public'));

    return app;
};