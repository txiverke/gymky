/**
 * Gymky.me
 * Created by xavi on 12/10/15.
 */

(function(){

    'use strict';
    angular
        .module('app.users')
        .service('AuthenticationService', [
            '$cookies',
            AuthenticationSrvFn
        ]);

    function AuthenticationSrvFn( $cookies ) {

        var self = this;
        var services = {
            getUser : getUser,
            cleanCookies: cleanCookies
        };

        /**
         * Check if user is logged
         * @returns {{user: (user|*)}}
         */
        function getUser() {

            self.user = window.user;

            return {
                user: self.user
            };

        }

        function cleanCookies() {
            console.log('esborra')
            $cookies.remove('gymkySession');
            $cookies.remove('gymkySession.sig');
        }

        return services;

    }

})();

