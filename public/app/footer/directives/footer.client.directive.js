/**
 * Gymky.me
 * Created by xavi on 13/02/16.
 */

(function () {

    'use strict';

    angular
        .module('app.footer')
        .directive('mainFooter', [ '$window', MainFooterFn ]);

    function MainFooterFn () {

        var controller = [
            '$scope',
            '$rootScope',
            '$cookies',
            MainFooterCtrlFn
        ];

        function MainFooterCtrlFn ($scope, $rootScope, $cookies) {

            var userLanguage = $cookies.getObject('userLang');

            if (angular.isDefined(userLanguage)) {

                $scope.currentLanguage = userLanguage;

            }

            $scope.year = new Date().getFullYear();
            $scope.languages = [
                { value  : 'ENGLISH', abbr : 'en_US' },
                { value  : 'ESPAÑOL', abbr : 'es' }
            ];

            $scope.setLanguage = function () {

                var lang = String($scope.currentLanguage.trim());

                $rootScope.$broadcast('userLang', lang );

            };

            if ($scope.year !== new Date().getFullYear()) {

                $scope.year = '2016 - ' + new Date().getFullYear();

            }

        }


        return {
            restrict: 'E',
            templateUrl: '/app/footer/views/footer.client.view.html',
            controller: controller
        };

    }

})();
