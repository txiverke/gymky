/**
 * Gymky.me
 * Created by xavi on 30/01/16.
 */

(function(){

    'use strict';
    angular
        .module('app.locale')
        .service('LocaleSrv', [
            '$translate',
            'LOCALES',
            '$rootScope',
            'tmhDynamicLocale',
            LocaleSrvFn
        ]);

    function LocaleSrvFn( $translate, LOCALES, $rootScope, tmhDynamicLocale ) {

        var localesObj = LOCALES.locales;
        var _LOCALES = Object.keys(localesObj);
        var _LOCALES_DISPLAY_NAMES = [];
        var currentLocale;
        var services = {
                checkLocaleIsValid      : checkLocaleIsValid,
                getLocaleDisplayName    : getLocaleDisplayName,
                setLocaleByDisplayName  : setLocaleByDisplayName,
                getLocalesDisplayNames  : getLocalesDisplayNames
            };

        if (!_LOCALES || _LOCALES.length === 0) {
            console.error('There are no _LOCALES provided');
        }

        _LOCALES.forEach(function ( locale ) {
            _LOCALES_DISPLAY_NAMES.push(localesObj[locale]);
        });

        currentLocale = $translate.proposedLanguage();
        currentLocale = currentLocale === undefined ? _LOCALES[0] : currentLocale;

        $rootScope.$on('$translateChangeSuccess', function (event, data) {

            document.documentElement.setAttribute('lang', data.language);
            tmhDynamicLocale.set(data.language.toLowerCase().replace(/_/g, '-'));

        });

        return services;

        function checkLocaleIsValid(locale) {
            return _LOCALES.indexOf(locale) !== -1;
        }

        function setLocale(locale) {
            if (!checkLocaleIsValid(locale)) {
                console.error('Locale name "' + locale + '" is invalid');
                return;
            }
            currentLocale = locale;
            $translate.use(locale);
        }

        function getLocaleDisplayName() {
            return localesObj[currentLocale];
        }

        function setLocaleByDisplayName(localeDisplayName) {
            setLocale(localeDisplayName);
        }

        function getLocalesDisplayNames() {
            return _LOCALES_DISPLAY_NAMES;
        }

    }

})();