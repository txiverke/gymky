/**
 * Gymky.me
 * Created by xavi on 26/12/15.
 */

(function(){

    'use strict';
    angular
        .module('gymkyApp')
        .run([
            '$rootScope',
            '$state',
            '$cookies',
            'AuthenticationService',
            'LocaleSrv',
            'LOCALES',
            appRunFn
        ]);

    function appRunFn( $rootScope, $state, $cookies, Authentication, LocaleSrv, LOCALES ){

        var authentication =  Authentication;
        var browserLanguage = navigator.language;
        var userLanguage = $cookies.getObject('userLang');

        if ( angular.isDefined( userLanguage ) ) {

            __defineLanguage( userLanguage );

        } else {

            __defineLanguage( browserLanguage );

        }

        $rootScope.$on('userLang', function( event, data ){

            $cookies.putObject('userLang', data);

            __defineLanguage( data );

        });

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

            if ( toState.requiredLogin && authentication.user === null ) {
                $state.go('login');
            }

        });

        function __defineLanguage( lang ) {

            if ( !LocaleSrv.checkLocaleIsValid( lang ) ) {
                browserLanguage = LOCALES.preferredLocale;
            }

            if ( lang !== 'en_US' && lang !== 'es') {
                browserLanguage = 'en_US';
            } else {
                browserLanguage = lang;
            }

            LocaleSrv.setLocaleByDisplayName( browserLanguage );

        }

    }


})();
