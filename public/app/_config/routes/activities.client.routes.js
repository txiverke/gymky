/**
 * Gymky.me
 * Created by xavi on 13/10/15.
 */

(function() {

    'use strict';
    angular
        .module('app.activities')
        .config([
            '$stateProvider',
            ActivitiesRouteFn
        ]);

    function ActivitiesRouteFn( $stateProvider ) {

        var ROOT = 'app/activities/views/';

        $stateProvider
            .state('activities', {
                abstract: true,
                templateUrl: ROOT + 'activities.client.view.html'
            })
            .state('activities.list', {
                url: '/activities',
                requiredLogin: true,
                templateUrl: ROOT + 'list-activities.client.view.html'
            })
            .state('activities.create', {
                url: '/activities/create',
                requiredLogin: true,
                templateUrl: ROOT + 'create-activity.client.view.html'
            })
            .state('activities.teams', {
                url: '/activities/teams/:activityId',
                params: { activityId: null },
                requiredLogin: true,
                templateUrl: ROOT + 'teams-activity.client.view.html'
            })
            .state('activities.tasks', {
                url: '/activities/tasks/:activityId',
                params: { activityId: null },
                requiredLogin: true,
                templateUrl: ROOT + 'tasks-activity.client.view.html'
            })
            .state('activities.background', {
                url: '/activities/background/:activityId',
                params: { activityId: null },
                requiredLogin: true,
                templateUrl: ROOT + 'background-activity.client.view.html'
            })
            .state('activities.view', {
                url: '/activities/:activityId',
                params: { activityId: null },
                requiredLogin: true,
                templateUrl: ROOT + 'view-activity.client.view.html'
            })
            .state('activities.edit', {
                url: '/activities/:activityId/edit',
                params: { activityId: null },
                requiredLogin: true,
                templateUrl: ROOT + 'edit-activity.client.view.html'
            });

    }

})();

