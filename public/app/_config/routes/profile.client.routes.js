/**
 * Gymky.me
 * Created by xavi on 15/11/15.
 */

(function() {

    'use strict';
    angular
        .module('app.profile')
        .config([
            '$stateProvider',
            ProfileRouteFn
        ]);

    function ProfileRouteFn( $stateProvider ) {

        var ROOT = 'app/profile/views/';

        $stateProvider
            .state('profile', {
                abstract: true,
                templateUrl: ROOT + 'profile.client.view.html'
            })
            .state('profile.edit', {
                url: '/profile/:userId/edit',
                params: { userId: '@userId' },
                requiredLogin: true,
                templateUrl: ROOT + 'edit-profile.client.view.html'
            })

    }

})();
