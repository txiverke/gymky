/**
 * Gymky.me
 * Created by xavi on 12/10/15.
 */

(function() {

    'use strict';
    angular
        .module('app.login')
        .config([
            '$stateProvider',
            '$urlRouterProvider',
            LoginRouteFn
        ]);

    function LoginRouteFn($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('login', {
                url: '/',
                templateUrl: 'app/login/views/login.client.view.html'
            });

    }

})();

