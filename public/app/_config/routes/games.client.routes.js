/**
 * Gymky.me
 * Created by xavi on 27/12/15.
 */

(function() {

    'use strict';

    angular
        .module('app.games')
        .config(['$stateProvider', LoginRouteFn ]);

    function LoginRouteFn ($stateProvider) {

        var ROOT = 'app/games/views/';

        $stateProvider
            .state('game', {
                abstract: true,
                templateUrl: ROOT + 'games.client.view.html'
            })
            .state('game.login', {
                url: '/treasure-hunt/:activityId/login',
                params: {activityId: null},
                templateUrl: ROOT + 'games-login.client.view.html'
            })
            .state('game.introduction', {
                url: '/treasure-hunt/:activityTitle/introduction',
                params: {activityTitle: null},
                templateUrl: ROOT + 'games-introduction.client.view.html'
            })
            .state('game.play', {
                url: '/treasure-hunt/:activityTitle/play',
                params: {activityTitle: null},
                templateUrl: ROOT + 'games-play.client.view.html'
            })
            .state('game.result', {
                url: '/treasure-hunt/:activityTitle/result',
                params: {activityTitle: null},
                templateUrl: ROOT + 'games-result.client.view.html'
            })

    }

})();
