/**
 * Gymky.me
 * Created by xavi on 26/12/15.
 */

(function(){

    'use strict';
    angular
        .module('gymkyApp')
        .constant('LOCALES', {
            'locales': {
                'en_US' : 'English',
                'es'    : 'Spanish'
            },
            'preferredLocale'  : 'en_US'
        })
        .config([
            '$locationProvider',
            '$translateProvider',
            'tmhDynamicLocaleProvider',
            ConfigFn
        ]);

    function ConfigFn( $locationProvider, $translateProvider, tmhDynamicLocaleProvider ){

        $locationProvider.hashPrefix('!');

        /** Translations */
        $translateProvider.useMissingTranslationHandlerLog();
        $translateProvider.useSanitizeValueStrategy('escapeParameters');
        $translateProvider.useStaticFilesLoader({
            prefix: '/translations/locale-',
            suffix: '.json'
        });
        $translateProvider.preferredLanguage('en_US');
        $translateProvider.use('en_US');

        tmhDynamicLocaleProvider.localeLocationPattern('/js/angular-i18n/angular-locale_en-us.js', '/js/angular-i18n/angular-locale_es.js');

    }

    if (window.location.hash === '#_=_') window.location.hash = '#!';

})();