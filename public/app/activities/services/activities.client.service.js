/**
 * Gymky.me
 * Created by xavi on 13/10/15.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .factory('ActivitiesService', [
            '$resource',
            ActivitiesSrvFn
        ]);

    function ActivitiesSrvFn ($resource) {

        var services = {};

        services.activity = $resource('api/activities/:activityId',
            {
                'activityId' : '@_id'
            },
            {
                'get'   :   { method : 'GET' },
                'save'  :   { method : 'POST' },
                'update':   { method : 'PUT' },
                'query' :   { method : 'GET', isArray: false },
                'remove':   { method:'DELETE' },
                'delete':   { method:'DELETE' }
            }
        );

        services.team = $resource('api/activities/:activityId/teams',
            { 'activityId' : '@_id' },
            { 'get' : { method: 'GET' } }
        );

        services.game = $resource('api/activities/login/:activityId/:leaderEmail',
            {
                'activityId'    : '@activityId',
                'leaderEmail'   : '@leaderEmail'
            },
            {
                'get' : { method: 'GET' }
            }
        );

        services.emails = $resource('api/activities/email/:activityId/:activityTitle',
            {
                'activityId' : '@_activityId',
                'activityTitle' : '@_activityTitle'
            },
            { 'get' : { method: 'GET' } }
        );

        services.task = $resource('api/activities/task/:activityId/:task',
            {
                'activityId': '@_id',
                'task'      : '@task',
            },
            {
                'get' : { method: 'GET' }
            }
        );

        services.question = $resource('api/activities/question/:activityId/:task/:response/:userId',
            {
                'activityId': '@_id',
                'task'      : '@taskId',
                'response'  : '@response',
                'userId'    : '@userId'
            },
            {
                'get'   : { method: 'GET' },
                'update': { method: 'PUT' }
            }
        );


        return services;

    }

})();

