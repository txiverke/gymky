/**
 * Gymky.me
 * Created by xavi on 15/12/15.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .controller('CreateTeamsCtrl', [
            '$scope',
            '$state',
            '$stateParams',
            '$timeout',
            '$rootScope',
            'AuthenticationService',
            'ActivitiesService',
            CreateTeamsCtrlFn
        ]);

    function CreateTeamsCtrlFn ($scope, $state, $stateParams, $timeout, $rootScope, AuthenticationSrv, ActivitiesSrv) {

        var self = this;
        self.authentication = AuthenticationSrv.getUser();
        self.editingActivity = $rootScope.editActivity;
        self.showButton = true;
        self.success = false;
        self.loading = true;

        __checkAuthentication();

        $rootScope.currentSection = 2;

        self.responseHandlers = {

            setActivityData: function (data) {

                self.activity = data;
                self.teams = self.activity.team;
                $scope.activity = self.activity;
                self.loading = false;

            },
            addTeam: function () {

                if (angular.isDefined(self.teams)) {
                    __cleanFields();
                    self.loading = false;
                }

            },
            createTasks: function (response) {

                self.loading = false;
                self.success = true;

                $timeout(function () {
                    $state.go('activities.tasks', { activityId: response._id });
                }, 1500);

            },
            getErrors: function () {

                self.error = 'Ups, Something was wrong!';
                self.loading = false;

            }

        };

        self.findActivity = function () {

            var activityId = { activityId: $stateParams.activityId };

            ActivitiesSrv.activity.get(
                activityId,
                self.responseHandlers.setActivityData,
                self.responseHandlers.getErrors
            );

        };

        self.addTeam = function () {

            if (!__checkFields()) {
                return;
            }

            self.loading = true;

            var teams = self.activity.team;
            var object = {
                title   : self.team_name,
                leader  : self.team_leader,
                email   : self.team_email
            };

            teams.push(object);

            self.teams = teams;
            $scope.activity.team = self.teams;

            self.responseHandlers.addTeam();

        };

        self.goToTasks = function () {

            if (self.activity.team.length > 0) {

                self.loading = true;
                self.activity.team = self.teams;

                self.activity.$update(
                    self.responseHandlers.createTasks,
                    self.responseHandlers.getErrors
                );

            }

        };

        /**
         * Check User Authenctication
         * @private
         */
        function __checkAuthentication () {

            if (self.authentication.user === null) {
                $state.go('login');
            }

        }

        /**
         * Check Fields Value
         * @returns {boolean|*}
         * @private
         */
        function __checkFields () {

            return  angular.isDefined(self.team_name) &&
                    angular.isDefined(self.team_email) &&
                    angular.isDefined(self.team_leader);

        }

        /**
         * Clean the Fields up
         * @private
         */
        function __cleanFields () {

            self.team_leader = undefined;
            self.team_name = undefined;
            self.team_email = undefined;

        }

    }

})();
