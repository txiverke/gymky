/**
 * Gymky.me
 * Created by xavi on 15/12/15.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .controller('ListActivitiesCtrl', [
            '$state',
            'AuthenticationService',
            'ActivitiesService',
            ListActivitiesCtrlFn
        ]);

    function ListActivitiesCtrlFn ($state, AuthenticationSrv, ActivitiesSrv) {

        var self = this;
        self.authentication = AuthenticationSrv.getUser();
        self.loading = true;

        __checkAuthentication();

        self.responseHandlers = {

            setActivityData: function (data) {

                var activities = data.activities;

                if (activities.length === 0) {
                    self.activities = activities;
                    self.loading = false;
                }

                for (var i = 0; i < activities.length; i++) {

                    var formatDate = new Date(activities[i].activity_date);
                    var state = activities[i].state;

                    if (state === 'Open' && new Date() >= formatDate) {

                        __updateState(activities[i]);

                    } else {
                        self.activities = activities;
                        self.loading = false;
                    }
                }

            },
            getErrors: function (data) {
                self.error = data;
                self.loading = false;
            }

        };

        self.findActivities = function () {

            __findActivities();

        };

        self.deleteActivity = function (activityId) {

            if ( window.confirm('This action will remove the Activity, Are you sure?') ) {

                self.loading = true;
                var defaultParams = { activityId: activityId };

                self.activity = ActivitiesSrv.activity.get(
                    defaultParams,
                    __deleteActivity,
                    self.responseHandlers.getErrors()
                );

            }

        };

        self.sendMails = function (activityId) {

            if ( window.confirm('You are going to send mails to Team Leaders to inform them about the Activity, Are you sure?') ) {

                self.loading = true;

                var defaultParams = { activityId: activityId };

                ActivitiesSrv.emails.get(
                    defaultParams,
                    self.responseHandlers.finishActivity,
                    self.responseHandlers.getErrors
                );
            }

        };

        /**
         * Delete Activity
         * @private
         */
        function __deleteActivity () {

            self.loading = true;
            self.activity.$remove( function () {
                __findActivities();
            });

        }

        /**
         * Get Activities
         * @private
         */
        function __findActivities () {

            ActivitiesSrv.activity.query(
                self.responseHandlers.setActivityData,
                self.responseHandlers.getErrors
            );

        }

        /**
         * Check User Authentication
         * @private
         */
        function __checkAuthentication () {

            if (self.authentication.user === null) {
                $state.go('login');
            }

        }

        /**
         * Update Activity Status
         * @param data
         * @private
         */
        function __updateState (data) {

            var activity = data;
            activity.state = 'Active';

            ActivitiesSrv.activity.update(
                activity,
                self.findActivities,
                self.responseHandlers.getErrors
            );

        }

    }

})();
