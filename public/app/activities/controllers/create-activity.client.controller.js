/**
 * Gymky.me
 * Created by xavi on 14/12/15.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .controller('CreateActivityCtrl', [
            '$state',
            '$timeout',
            '$rootScope',
            'AuthenticationService',
            'ActivitiesService',
            CreateActivityCtrlFn
        ]);

    function CreateActivityCtrlFn ($state, $timeout, $rootScope, AuthenticationSrv, ActivitiesSrv) {

        var self = this;
        self.authentication = AuthenticationSrv.getUser();
        self.success = false;
        self.activity_date = new Date();
        self.minDate = new Date(
            self.activity_date.getFullYear(),
            self.activity_date.getMonth(),
            self.activity_date.getDate()
        );

        __checkAuthentication();

        $rootScope.currentSection = 1;
        $rootScope.editActivity = false;

        /**
         * Handle API Rest responses
         * @type {{createActivity: Function, getErrors: Function}}
         */
        self.responseHandlers = {

            createActivity: function (response) {

                self.loading = false;
                self.success = true;

                $timeout(function () {
                    $state.go('activities.teams', { activityId: response._id });
                }, 1500);

            },
            getErrors: function (error) {

                self.error = 'Ups, Something was wrong! ' + error;
                self.loading = false;

            }

        };

        self.createTeams = function () {

            var title = self.title;
            var activity_date = self.activity_date;

            if (!__validateActivity (title, activity_date)) {
                return;
            }

            self.loading = true;

            var activity = new ActivitiesSrv.activity({
                title : title,
                state : 'Open',
                activity_date : activity_date
            });

            activity.$save(
                self.responseHandlers.createActivity,
                self.responseHandlers.getErrors
            );

        };

        self.cancelCreation = function () {

          $state.go('activities.list');

        };

        /**
         * Check User Authentication
         * @private
         */
        function __checkAuthentication () {

            if (self.authentication.user === null) {
                $state.go('login');
            }

        }

        /**
         * Validate Activity Fields
         * @param title
         * @param activity_date
         * @returns {boolean}
         * @private
         */
        function __validateActivity (title, activity_date) {

            if (!angular.isString(title) || title.length === 0) {
                return false;
            } else if (!angular.isDate( activity_date)) {
                return false;
            }
            return true;
        }

    }

})();
