/**
 * Gymky.me
 * Created by xavi on 04/01/16.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .controller('CreateBackgroundCtrl', [
            '$stateParams',
            '$state',
            '$rootScope',
            '$timeout',
            'Upload',
            'ActivitiesService',
            'AuthenticationService',
            CreateBackgroundCtrlFn
        ]);

    function CreateBackgroundCtrlFn ($stateParams, $state, $rootScope, $timeout, Upload, ActivitiesSrv, AuthenticationSrv) {

        var self = this;
        self.authentication = AuthenticationSrv.getUser();
        self.editingActivity = $rootScope.editActivity;
        self.picFile = null;
        self.success = false;
        self.loading = true;

        __checkAuthentication();

        $rootScope.currentSection = 4;

        self.responseHandlers = {

            setActivityData: function (data) {

              self.activity = data;
              self.loading = false;

            },
            uploadBackground: function () {

                self.loading = false;
                self.success = true;

                __findActivity();

                $timeout( function () { self.success = false; }, 1500 );

            },
            finishActivity: function () {

                self.loading = false;
                self.success = true;

                $timeout( function () { $state.go('activities.list'); }, 1500 );
            },
            getError: function (error) {

                self.error = error;

            }

        };

        self.findActivity = function () {

            __findActivity();

        };

        self.uploadBackground = function (image) {

            if (angular.isArray(image)) {
                image = image[0];
            }

            if (image.type !== 'image/png' && image.type !== 'image/jpeg') {
                var errorResponse = 'Only PNG and JPEG are accepted.';
                self.responseHandlers.getError( errorResponse );
            }

            self.loading = true;
            self.uploadInProgress = true;

            var activityId = $stateParams.activityId;
            var defaultParams = {
                url: 'api/activities/' + activityId + '/background',
                method: 'PUT',
                data: { file : image }
            };

            Upload.upload(defaultParams)
                .success(function (data) {
                    self.responseHandlers.uploadBackground();
                })
                .error(function (err) {

                    var error = 'Error uploading file: ' + err.message || err;

                    self.uploadInProgress = false;
                    self.loading = false;
                    self.responseHandlers.getError(error);
                }
            );

        };

        self.finishActivity = function() {

            if (self.picFile === null && angular.isUndefined(self.activity.background)) {
                self.activity.background = 'people.jpg';
            }

            self.loading = true;
            self.activity.$update(function () {
                self.responseHandlers.finishActivity();
            }, function (errorResponse) {
                self.error = errorResponse.data.message;
            });

        };

        /**
         * Check User Authentication
         * @private
         */
        function __checkAuthentication () {

            if (self.authentication.user === null) {
                $state.go('login');
            }

        }

        /**
         * Get Activities
         * @private
         */
        function __findActivity () {

            var activityId = { activityId: $stateParams.activityId };

            ActivitiesSrv.activity.get(
                activityId,
                self.responseHandlers.setActivityData,
                self.responseHandlers.getErrors
            );

        }

    }

})();
