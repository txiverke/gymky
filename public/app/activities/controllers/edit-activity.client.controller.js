/**
 * Gymky.me
 * Created by xavi on 02/01/16.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .controller('EditActivityCtrl', [
            '$state',
            '$stateParams',
            '$rootScope',
            '$timeout',
            'ActivitiesService',
            'AuthenticationService',
            EditActivityCtrlFn
        ]);

    function EditActivityCtrlFn ($state, $stateParams, $rootScope, $timeout, ActivitiesSrv, AuthenticationSrv) {

        var self = this;
        self.authentication = AuthenticationSrv.getUser();
        self.loading = true;

        __checkAuthentication();

        $rootScope.currentSection = 1;
        $rootScope.editActivity = true;

        self.responseHandlers = {

            setActivityData: function (data) {

                self.activity = data;
                self.activity_date = new Date(data.activity_date);
                self.minDate = new Date(
                    new Date().getFullYear(),
                    new Date().getMonth(),
                    new Date().getDate()
                );
                self.loading = false;

            },
            editTeams: function (data) {

                self.loading = false;
                self.success = true;

                $timeout(function(){
                    $state.go('activities.teams', { activityId: data._id });
                }, 1500);


            },
            getErrors: function (data) {
                self.loading = false;
                self.error = data;
            }

        };

        self.findActivity = function () {

            var activityId = { activityId: $stateParams.activityId };

            console.log(activityId);

            ActivitiesSrv.activity.get(
                activityId,
                self.responseHandlers.setActivityData,
                self.responseHandlers.getErrors
            );

        };

        self.editTeams = function () {

            if (!__checkFields()) {
                return;
            }

            self.loading = true;
            self.activity.activity_date = self.activity_date;

            self.activity.$update(
                self.responseHandlers.editTeams,
                self.responseHandlers.getErrors
            );

        };

        self.cancelEdition = function () {

            $rootScope.editActivity = false;

            $state.go('activities.list');

        };

        /**
         * Check User Authentication
         * @private
         */
        function __checkAuthentication () {

            if (self.authentication.user === null) {
                $state.go('login');
            }

        }

        /**
         * Check Fields Value
         * @returns {boolean}
         * @private
         */
        function __checkFields () {

            if (!angular.isString(self.activity.title ) || !self.activity.title) {
                return false;
            } else if (!angular.isDate(self.activity_date)){
                return false;
            }
            return true;

        }

    }

})();
