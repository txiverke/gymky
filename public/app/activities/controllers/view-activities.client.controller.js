/**
 * Gymky.me
 * Created by xavi on 13/10/15.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .controller('ViewActivityController', [
            '$stateParams',
            '$state',
            '$scope',
            'AuthenticationService',
            'ActivitiesService',
            ViewActivityCtrlFn
        ]);

    function ViewActivityCtrlFn ($stateParams, $state, $scope, AuthenticationSrv, ActivitiesSrv) {

        var self = this;
        self.authentication = AuthenticationSrv.getUser();

        __checkAuthentication();

        self.findActivity = function () {

            var activityId = { activityId: $stateParams.activityId };

            self.activity = ActivitiesSrv.activity.get( activityId );
            $scope.activity = self.activity;

        };

        self.deleteActivity = function () {

            console.log(self.activity);

            /*self.activity.$remove(function() {
                $state.go('activities.list');
            });*/

        };

        /**
         * Check User Authentication
         * @private
         */
        function __checkAuthentication () {

            if (self.authentication.user === null) {
                $state.go('login');
            }

        }

        /**
         * @todo Add a directive to handle this feature
         */
        $(window).scroll(function () {
            var oVal;
            oVal = $(window).scrollTop() / 200;
            return $('.blur').css('opacity', oVal);
        });

    }

})();
