/**
 * Gymky.me
 * Created by xavi on 15/12/15.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .controller('CreateTasksCtrl', [
            '$scope',
            '$state',
            '$stateParams',
            '$timeout',
            '$rootScope',
            'AuthenticationService',
            'ActivitiesService',
            CreateTasksCtrlFn
        ]);

    function CreateTasksCtrlFn ($scope, $state, $stateParams, $timeout, $rootScope, AuthenticationSrv, ActivitiesSrv) {

        var self = this;
        self.authentication = AuthenticationSrv.getUser();
        self.editingActivity = $rootScope.editActivity;
        self.showButton = true;
        self.success = false;
        self.loading = true;
        self.list_of_tasks = [
            { value : 'Question', sentence: 'Write a question' },
            { value : 'Multiple', sentence: 'Multiple responses' },
            { value : 'Picture', sentence: 'Upload a picture' },
            { value : 'Clue', sentence: 'Add a clue'}
            /* @todo Add Position Feature
             { value : 'Position', sentence: 'Find a place' }
             */
        ];

        $scope.list_of_tasks = self.list_of_tasks;

        __checkAuthentication();

        $rootScope.currentSection = 3;

        self.responseHandlers = {

            setActivityData: function (data) {

                self.activity = data;
                self.tasks = self.activity.task;
                $scope.activity = self.activity;
                self.loading = false;

            },
            addTask: function () {

              if (angular.isDefined(self.tasks)) {
                  __cleanFields();
                  self.loading = false;
              }

            },
            goToBackground: function () {

                self.error = false;
                self.loading = false;
                self.success = true;

                $timeout(function () {
                    $state.go('activities.background', { activityId: $stateParams.activityId });
                }, 1500);

            },
            getErrors: function () {

                self.error = 'Ups, Something was wrong!';
                self.loading = false;

            }

        };

        self.findActivity = function () {

            var activityId = { activityId: $stateParams.activityId };

            ActivitiesSrv.activity.get(
                activityId,
                self.responseHandlers.setActivityData,
                self.responseHandlers.getErrors
            );

        };

        self.addTask = function () {

            if (!__checkFields()) {
                return;
            }

            self.loading = true;

            switch (self.kind_of) {

                case 'Multiple':
                    __setMultipleFormat();
                    break;

                case 'Question':
                    __setQuestionFormat();
                    break;

            }

            var tasks = self.activity.task;
            var taskId = self.tasks !== undefined ? self.tasks.length : 0;
            var object = {
                id          : taskId,
                kind_of     : self.kind_of,
                title       : self.title,
                response    : self.response,
                responses   : self.activity.task.responses,
                img         : self.activity.task.id,
                state       : 'Active',
                position    : self.activity.task.position
            };

            tasks.push(object);

            self.tasks = tasks;
            $scope.activity.task = self.tasks;

            self.responseHandlers.addTask();

        };

        self.goToBackground = function () {

            if (self.activity.task.length > 0) {

                self.loading = true;
                self.activity.task = self.tasks;

                self.activity.$update(
                    self.responseHandlers.goToBackground,
                    self.responseHandlers.getErrors
                );

            }

        };

        /**
         * Set Format for Questions with one response
         * @private
         */
        function __setQuestionFormat () {

            self.response = self.response.toLowerCase();

        }

        /**
         * Set Format for Questions with Multiple Responses
         * @private
         */
        function __setMultipleFormat () {

            var responses = [];

            responses.push(
                { value: self.right_response.toLowerCase(), state: true },
                { value: self.wrong_response1.toLowerCase(), state: false },
                { value: self.wrong_response2.toLowerCase(), state: false }
            );

            self.activity.task.responses = responses;

        }


        /**
         * Check User Authentication
         * @private
         */
        function __checkAuthentication () {

            if (self.authentication.user === null) {
                $state.go('login');
            }

        }

        /**
         * Check id kind of Task is defined
         * @returns {boolean|*}
         * @private
         */
        function __checkFields () {

            return angular.isDefined(self.kind_of);

        }

        /**
         * Clean up the Fields
         * @private
         */
        function __cleanFields () {

            self.title = undefined;
            self.response = undefined;
            self.activity.task.responses = undefined;
            self.kind_of = undefined;

        }

    }

})();