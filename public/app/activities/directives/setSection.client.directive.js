/**
 * Gymky.me
 * Created by xavi on 19/12/15.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .directive('setSection',[
            '$rootScope',
            '$filter',
            setSectionFn
        ]);

    function setSectionFn ($rootScope, $filter) {

        function link( scope, elem, attr ){

            var section = $rootScope.currentSection;

            switch (section) {

                case 1:
                    scope.message = $filter('translate')('ADD_ACTIVITY_MSG');
                    break;
                case 2:
                    scope.message = $filter('translate')('ADD_TEAMS_MSG');
                    break;
                case 3:
                    scope.message = $filter('translate')('CREATE_TASKS_MSG');
                    break;
                case 4:
                    scope.message = $filter('translate')('ADD_BACKGROUND_MSG');
                    break;
            }

            $('#section-status').find('.status-' + section).addClass('active');

        }

        return {
            restrict: 'EA',
            templateUrl: '/app/activities/partials/setSection.client.partial.html',
            link: link
        };

    }

})();
