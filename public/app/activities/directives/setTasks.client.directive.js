/**
 * Gymky.me
 * Created by xavi on 19/12/15.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .directive('setTasks', [ setTasksFn ]);

    function setTasksFn(){

        function link(scope, elem, attr) {

            scope.limit = 20;

            scope.remove = function (array, index) {
                array.splice( index, 1 );
            };

        }

        return {
            restrict: 'EA',
            templateUrl: '/app/activities/partials/setTasks.client.partial.html',
            link: link
        };

    }


})();
