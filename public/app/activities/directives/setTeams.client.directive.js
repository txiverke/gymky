/**
 * Gymky.me
 * Created by xavi on 15/10/15.
 */

(function () {

    'use strict';

    angular
        .module('app.activities')
        .directive('setTeams', [ setTeamsFn ]);

    function setTeamsFn () {

        function link (scope, elem, attr) {

            scope.remove = function(array, index) {
                array.splice(index, 1);
            };

        }

        return {
            restrict: 'EA',
            templateUrl: '/app/activities/partials/setTeams.client.partial.html',
            link: link
        };

    }


})();
