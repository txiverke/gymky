/**
 * Gymky.me
 * Created by xavi on 30/12/15.
 */

(function () {

    'use strict';

    angular
        .module('app.games')
        .controller('IntroductionGameCtrl', [
            '$cookies',
            '$state',
            '$filter',
            'ActivitiesService',
            IntroductionGameCtrlFn
        ]);

    function IntroductionGameCtrlFn ($cookies, $state, $filter, ActivitiesSrv){

        var self = this;
        var user = $cookies.getObject('currentUser');
        var url;

        console.log(user);

        self.loading = true;

        /**
         * Handles API Rest Responses
         * @type {{getActivity: Function, noAuthenticated: Function, getErrors: Function}}
         */
        self.responseHandlers = {

            getActivity: function (data) {

                data.leader = user.leader;
                data.teamName = user.teamName;
                self.activity = data;
                console.log(data)
                self.loading = false;

            },
            noAuthenticated: function () {

                $state.go('login');

            },
            getErrors: function (data) {

                self.error = data;

            }

        };

        self.findActivity = function () {

            if (__checkAuthentication()) {

                var defaultParams = { activityId: user.activityId };
                url  = $filter('prettyUrl')(user.gameTitle);

                ActivitiesSrv.team.get(
                    defaultParams,
                    self.responseHandlers.getActivity,
                    self.responseHandlers.noAuthenticated
                );
            }

        };

        self.startGame = function () {

            self.loading = true;
            $state.go('game.play', { activityTitle: url });

        };

        self.cancelGame = function () {

            $state.go('login');

        };

        /**
         * Check User Authentication
         * @private
         */
        function __checkAuthentication () {

            if (angular.isUndefined(user)) {

                self.responseHandlers.noAuthenticated();
                return false;

            }

            return true;

        }

    }

})();
