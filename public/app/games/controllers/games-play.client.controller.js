/**
 * Gymky.me
 * Created by xavi on 07/01/16.
 */

(function(){

    'use strict';
    angular
        .module('app.games')
        .controller('PlayGameCtrl', [
            '$scope',
            '$state',
            '$filter',
            '$cookies',
            '$timeout',
            'Upload',
            'ActivitiesService',
            'GamesService',
            PlayGameCtrlFn
        ]);

    function PlayGameCtrlFn ($scope, $state, $filter, $cookies, $timeout, Upload, ActivitiesSrv, GamesSrv) {

        var self = this;
        var user = $cookies.getObject('currentUser');
        var totalTasks;
        var gameCompleted;
        var url;

        console.log(user)

        self.success = false;
        self.failed = false;
        self.picFile = null;
        self.gameCompleted = false;

        /**
         * Handles API Rest Responses
         * @type {{setTask: Function, checkResponse: Function, updateTask: Function, setResults: Function, getErrors: Function, noAuthenticated: Function}}
         */
        self.responseHandlers = {

            setTask : function (data) {

                url  = $filter('prettyUrl')(user.gameTitle);

                if (Number(data.id) === Number(totalTasks - 1)) {
                    gameCompleted = true;
                }

                if (!data.result) {

                    var kind_of_task = data.kind_of;

                    __setTaskScope(data);

                    switch (kind_of_task) {
                        case 'Question':
                            __setQuestionTask(data);
                            break;
                        case 'Multiple':
                            __setMultipleTask(data);
                            break;
                        case 'Picture':
                            __setPictureTask(data);
                            break;
                        case 'Position':
                            __setPositionTask(data);
                            break;
                        case 'Clue':
                            __setQuestionTask(data);
                            break;
                    }

                } else {

                    $state.go('game.result', { activityTitle: url });

                }


            },
            checkResponse : function (data) {

                __saveGameResponse(data);

            },
            updateTask : function (data) {

                self.loading = false;

                 if (data.response) {
                 __updateTask(true)
                 } else {
                 __updateTask(false)
                 }

            },
            setResults : function (data) {

                self.result = data;
                self.loading = false;

            },
            getErrors : function (data) {

                self.error = data;
                self.loading = false;

            },
            noAuthenticated : function () {
                $state.go('login');
            }

        };

        self.setTask = function() {

            self.loading = true;

            if (__checkAuthentication()) {

                var defaultParams = {
                    gameId : user.gameId
                };

                GamesSrv.getTaskId.get(
                    defaultParams,
                    __getTask,
                    self.responseHandlers.getErrors
                )

            }

        };

        self.checkResponse = function (response) {

            self.loading = true;

            var defaultParams = {
                activityId  : user.activityId,
                task        : self.task.id,
                response    : response,
                userId      : user.teamId
            };

            console.log(defaultParams)

            ActivitiesSrv.question.get(
                defaultParams,
                self.responseHandlers.checkResponse,
                self.responseHandlers.getErrors
            )

        };

        self.uploadBackground = function (image) {

            if (angular.isArray(image)) {
                image = image[0];
            }

            if (image.type !== 'image/png' && image.type !== 'image/jpeg') {
                var errorResponse = $filter('translate')('ERROR_IMAGE');
                self.responseHandlers.getError( errorResponse )
            }

            self.loading = true;
            self.uploadInProgress = true;

            var activityId = user.activityId;
            var task = self.task.id;
            var userId = user._id;
            var defaultParams = {
                url: 'api/activities/background/' + activityId + '/' + task + '/' + userId,
                method: 'PUT',
                data: { file : image }
            };

            Upload.upload(defaultParams)
                .success(function (data) {
                    self.responseHandlers.checkResponse(data);
                })
                .error(function (err) {
                    self.uploadInProgress = false;
                    console.log('Error uploading file: ' + err.message || err);
                }
            );

        };

        /**
         * Gets the Task data
         * @param data
         * @private
         */
        function __getTask (data) {

            if (__checkAuthentication()) {

                var taskId = (angular.isDefined(data)) ? Number(data.taskId) : 0;

                var defaultParams = {
                    activityId  : user.activityId,
                    task        : taskId
                };

                ActivitiesSrv.task.get(
                    defaultParams,
                    self.responseHandlers.setTask,
                    self.responseHandlers.getErrors
                );

            }

        }

        /**
         * Saves the Game Responses
         * @param data
         * @private
         */
        function __saveGameResponse (data) {

            var task = {
                id          : data.responses[0].id,
                kind_of     : data.responses[0].kind_of,
                title       : data.responses[0].title,
                state       : data.responses[0].state,
                response    : data.responses[0].response
            };

            var game = {
                activityId  : user.activityId,
                teamId      : user.teamId,
                teamName    : user.title,
                email       : user.email,
                leader      : user.leader,
                gameTitle   : user.gameTitle,
                task        : task,
                completed   : gameCompleted ? new Date() : null
            };

            var gameId = { gameId : user.gameId };

            GamesSrv.game.update(
                gameId,
                game,
                self.responseHandlers.updateTask,
                self.responseHandlers.getErrors
            )

        }

        function __updateTask( state ) {

            if (self.picFile) {
                self.picFile === null;
            }

            $timeout(function () {
                if (state) {
                    self.success = false;
                }else{
                    self.failed = false;
                }
            }, 1500);

            self.setTask();

        }

        /**
         * Set Single Question Data
         * @param task
         * @private
         */
        function __setQuestionTask (task) {

            self.response = undefined;
            self.task = task;
            self.loading = false;

        }

        /**
         * Set Multiple Question Data
         * @param task
         * @private
         */
        function __setMultipleTask( task ) {

            var responses = [];

            for ( var i=0; i < task.responses.length; i++ ){
                responses.push( task.responses[i].value );
                responses.sort();
            }

            self.responses = responses;
            self.task = task;
            self.loading = false;

        }

        /**
         * Set Picture Data
         * @param task
         * @private
         */
        function __setPictureTask( task ){

            self.picFile = undefined;
            self.task = task;
            self.loading = false;

        }

        function __setPositionTask( task ){

            self.task = task;
            self.loading = false;

        }

        /**
         * Sets Directives Scope up
         * @param data
         * @private
         */
        function __setTaskScope (data) {

            var task = [];

            $scope.id = Number(data.id);
            $scope.gameTitle = user.gameTitle;

            for (var i = 0; i < data.total; i ++) {

                task.push({
                    taskId : i,
                    icon: data.kind_of
                });

                $scope.total = task;

            }

        }

        /**
         * Check User Authentication
         * @private
         */
        function __checkAuthentication () {

            if (angular.isUndefined(user)) {

                self.responseHandlers.noAuthenticated();
                return false;

            }

            totalTasks = user.totalTasks;
            return true;

        }

    }

})();
