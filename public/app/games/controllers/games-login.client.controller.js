/**
 * Gymky.me
 * Created by xavi on 27/12/15.
 */

(function () {

    'use strict';

    angular
        .module('app.games')
        .controller('GameLoginController', [
            '$scope',
            '$stateParams',
            '$state',
            '$timeout',
            '$cookies',
            '$filter',
            'ActivitiesService',
            'GamesService',
            GameCtrlFn
        ]);

    function GameCtrlFn ($scope, $stateParams, $state, $timeout, $cookies, $filter, ActivitiesSrv, GamesSrv) {

        var self = this;
        var user = $cookies.getObject('currentUser');
        var currentUser;
        var url;

        self.loading = true;
        self.success = false;

        /**
         * Handle API Rest Responses
         * @type {{getActivity: Function, newUser: Function, oldUser: Function, goToPlay: Function, unknownActivity: Function, unknownLeader: Function}}
         */
        self.responseHandlers = {

            getActivity: function (data) {

                console.log('getActivity', data)

                url = $filter('prettyUrl')(data.title);

                $scope.$emit('setTimer', false);

                var formatDate = new Date(data.date);

                if (new Date() >= formatDate) {
                    self.available = true;
                    self.activity = data;
                    self.loading = false;
                } else {
                    self.message = $filter('translate')('ERROR_NO_YET_AVAILABLE');
                    self.available = false;
                    self.loading = false;
                }

            },
            newUser: function () {

                __createGame();

            },
            oldUser: function (data) {

                currentUser = {
                    activityId  : $stateParams.activityId,
                    teamId      : data.teamId,
                    teamName    : data.teamName,
                    email       : data.email,
                    leader      : data.leader,
                    gameTitle   : self.gameTitle
                };

                __createCookie(data);
                __goToPlay();

            },
            goToPlay: function (data) {

                var cookie = $cookies.getObject('currentUser');

                if (data.result && cookie) {

                     __goToPlay();

                } else if (data._id && !cookie) {

                    __createCookie(data);
                    __goToPlay();

                } else {

                    self.responseHandlers.unknownLeader();

                }

            },
            unknownActivity: function () {

                $state.go('login');

            },
            unknownLeader: function () {

                self.loading = false;
                self.error = $filter('translate')('ERROR_NO_ACCESS_GAME');

                __cleanFields();

            }

        };

        /**
         * Initializes the data of the Module
         */
        self.findActivity = function () {

            if (angular.isDefined($stateParams.activityId)) {

                var defaultParams = { activityId : $stateParams.activityId };

                ActivitiesSrv.team.get(
                    defaultParams,
                    self.responseHandlers.getActivity,
                    self.responseHandlers.unknownActivity
                );

            }

        };

        /**
         * Gets the email to check if is registered as a Team Leader
         */
        self.logInLeader = function () {

            self.loading = true;

            if (angular.isDefined(user) && self.email && user.email === self.email) {

                __gameSignIn();

            } else if (self.email) {

                var defaultParams = {
                    activityId  : $stateParams.activityId,
                    leaderEmail : self.email.trim()
                };

                ActivitiesSrv.game.get(
                    defaultParams,
                    __checkGameRegistration,
                    self.responseHandlers.unknownLeader
                );

            }

        };

        /**
         * Checks if user is a valid Team Leader
         * @private
         */
        function __gameSignIn () {

            var defaultParams = {
                activityId  : $stateParams.activityId,
                leaderEmail : self.email.trim()
            };

            ActivitiesSrv.game.get(
                defaultParams,
                self.responseHandlers.goToPlay,
                self.responseHandlers.unknownLeader
            );

        }

        /**
         * Check if the Game was registered before
         * @param data
         * @private
         */
        function __checkGameRegistration (data) {

            if (data.result) {

                currentUser = data.team;
                currentUser.activityId = $stateParams.activityId;
                currentUser.gameTitle = self.activity.title;

                var defaultParams = {
                    activityId  : $stateParams.activityId,
                    email : currentUser.email
                };

                GamesSrv.oldGame.get(
                    defaultParams,
                    self.responseHandlers.oldUser,
                    self.responseHandlers.newUser
                );

            } else {

                self.responseHandlers.unknownLeader(data);

            }

        }

        /**
         * Create a new Game Registration
         * @private
         */
        function __createGame () {

            if (angular.isDefined(currentUser)) {

                var game = new GamesSrv.game({
                    activityId  : currentUser.activityId,
                    teamId      : currentUser._id,
                    teamName    : currentUser.title,
                    email       : currentUser.email,
                    leader      : currentUser.leader,
                    gameTitle   : self.activity.title
                });

                game.$save(
                    self.responseHandlers.goToPlay,
                    self.responseHandlers.getErrors
                );

            }

        }

        /**
         * Go to the Introduction view
         * @private
         */
        function __goToPlay () {

            $timeout(function(){
                $state.go('game.introduction', { activityTitle : url });
            },1500);

        }

        /**
         * Create a Cookies with User and Game data
         * @param data
         * @private
         */
        function __createCookie (data) {

            console.log(data);

            currentUser.gameId = data._id;
            currentUser.gameTitle = self.activity.title;
            currentUser.totalTasks = self.activity.tasks;

            $cookies.putObject('currentUser', currentUser);

        }

        /**
         * Clean the email field up
         * @private
         */
        function __cleanFields () {

            self.email = undefined;

        }

        /**
         * @todo Create a directive to manage page scrolls
         */
        $(window).scroll(function () {
            var oVal;
            oVal = $(window).scrollTop() / 200;
            return $('.blur').css('opacity', oVal);
        });

    }

})();
