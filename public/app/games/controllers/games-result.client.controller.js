/**
 * Gymky.me
 * Created by xavi on 29/02/16.
 */

(function () {

    "use strict";
    angular
        .module('app.games')
        .controller('ResultGameCtrl', [
            '$state',
            '$cookies',
            'GamesService',
            ResultGameCtrlFn
        ]);

    function ResultGameCtrlFn ($state, $cookies, GamesSrv) {

        var self = this;
        var user = $cookies.getObject('currentUser');

        console.log(user)

        self.loading = true;

        self.responseHandlers = {

            setResults: function (data) {
                console.log(data);
                data.gameTitle = user.gameTitle;
                data.leader = user.leader;
                self.results = data;
                self.loading = false;
            },
            getErrors: function (err) {
                self.error = err;
                self.loading = false;
            },
            noAuthenticated: function () {
                $state.go('login');
            }

        };

        self.setResults = function () {

            if (__checkAuthentication()) {

                var defaultParams = {gameId: user.gameId};

                GamesSrv.results.get(
                    defaultParams,
                    self.responseHandlers.setResults,
                    self.responseHandlers.noAuthenticated
                )

            }

        };

        /**
         * Check User Authentication
         * @returns {boolean}
         * @private
         */
        function __checkAuthentication () {

            if (angular.isUndefined(user)) {

                self.responseHandlers.noAuthenticated();
                return false;

            }

            return true;

        }

    }

})();