/**
 * Gymky.me
 * Created by xavi on 08/01/16.
 */

(function(){

    'use strict';
    angular
        .module('app.games')
        .directive('setTask',[
            '$rootScope',
            setTaskFn
        ]);

    function setTaskFn( $rootScope ){

        function link( scope, elem, attr ){


        }

        return {
            restrict: 'EA',
            templateUrl: '/app/games/partials/setTask.client.partial.html',
            link: link
        }

    }

})();
