/**
 * Gymky.me
 * Created by xavi on 01/01/16.
 */

(function(){

    'use strict';

    angular
        .module('app.games')
        .factory('GamesService', [
            '$resource',
            GamesSrvFn
        ]);

    function GamesSrvFn( $resource ) {

        var services = {};

        services.game = $resource('api/games/:gameId',
            { 'gameId' : '@_gameId' },
            {
                'get'   :   { method : 'GET' },
                'save'  :   { method : 'POST' },
                'update':   { method : 'PUT' },
                'query' :   { method : 'GET', isArray: false },
                'remove':   { method : 'DELETE' },
                'delete':   { method : 'DELETE' }
            }
        );

        services.getTaskId = $resource('api/games/:gameId/taskId',
            { 'gameId': '@_gameId' },
            { 'get'   : { method : 'GET' } }
        );

        services.oldGame = $resource('api/games/:activityId/:email',
            { 'activityId': '@_activityId', 'email' : '@_email' },
            { 'get'   : { method : 'GET'} }
        );

        services.results = $resource('api/games/:gameId/results',
            { 'gameId': '@_gameId' },
            { 'query' : { method: 'GET', isArray: false } }
        );

        return services;

    }

})();
