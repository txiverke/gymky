/**
 * Gymky.me
 * Created by xavi on 09/01/16.
 */

(function(){

    'use strict';

    angular
        .module('app.menu')
        .directive('gameMenu', [ GameMenuFn ]);

    function GameMenuFn() {

        var controller = [
            '$scope',
            MainMenuCtrlFn
        ];

        function MainMenuCtrlFn( $scope ){

            $scope.$on('setTimer', function(event, data) {
                $scope.counter = data;
            });


        }

        function link( scope ){

            scope.limit = 35;

        }

        return {
            restrict: 'E',
            templateUrl: '/app/menu/views/menu-game.client.view.html',
            controller: controller,
            link: link
        }
    }

})();