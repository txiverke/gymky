/**
 * Gymky.me
 * Created by xavi on 15/11/15.
 */

(function(){

    'use strict';
    angular
        .module('app.menu')
        .directive('mainMenu', [ MainMenuFn ]);

    function MainMenuFn() {

        var controller = [
            '$scope',
            'ProfileService',
            'AuthenticationService',
            MainMenuCtrlFn
        ];

        function MainMenuCtrlFn( $scope, ProfileSrv, AuthenticationSrv ){

            var user = AuthenticationSrv.getUser();

            if (angular.isDefined( user ) && user.user !== null) {
                $scope.authentication = ProfileSrv.profile.get({ userId: user.user._id });
            }else{
                $scope.authentication = null;
            }

            $scope.cleanCookies = function() {
                AuthenticationSrv.cleanCookies();
            }

        }

        function link( scope ){

            var $elem = $('#menu-mobile');

            scope.showMenu = function() {
                $elem.find('.left-menu').addClass('left-menu-open');
                $elem.find('.menu-mask').addClass('dialog-bg');
            };

            scope.hideMenu = function() {
                $elem.find('.left-menu').removeClass('left-menu-open');
                $elem.find('.menu-mask').removeClass('dialog-bg');
            }

        }

        return {
            restrict: 'E',
            templateUrl: '/app/menu/views/menu.client.view.html',
            controller: controller,
            link: link
        }
    }

})();