/**
 * Gymky.me
 * Created by xavi on 13/10/15.
 */

(function(){

    'use strict';
    angular
        .module('app.login')
        .controller('LoginController', [ LoginCtrlFn ]);

    function LoginCtrlFn() {

        $(window).scroll(function () {
            var oVal;
            oVal = $(window).scrollTop() / 200;
            return $('.blur').css('opacity', oVal);
        });

    }

})();
