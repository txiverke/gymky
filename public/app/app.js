/**
 * Gymky.me
 * Created by xavi on 12/10/15.
 */

(function () {

    'use strict';
    angular
        .module('gymkyApp', [
            'ngResource',
            'ngMaterial',
            'tmh.dynamicLocale',
            'pascalprecht.translate',
            'ui.router',
            'ngCookies',
            'app.users',
            'app.login',
            'app.locale',
            'app.menu',
            'app.activities',
            'app.profile',
            'app.games',
            'app.footer',
            'app.filters'
        ]);

    angular
        .element(document).ready(function() {
            angular.bootstrap(document, ['gymkyApp']);
        });

})();

