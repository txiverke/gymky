/**
 * Gymky.me
 * Created by xavi on 20/02/16.
 */

(function(){

    'use strict';
    angular
        .module('app.filters')
        .filter('prettyUrl', PrettyUrlFn );

    function PrettyUrlFn(){

        return function ( string ){

            string = string.replace(/ +/g, '-').toLowerCase();

            return string;

        }

    }

})();
