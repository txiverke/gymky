/**
 * Gymky.me
 * Created by xavi on 15/11/15.
 */

(function() {

    'use strict';
    angular
        .module('app.profile')
        .factory('ProfileService', [
            '$resource',
            ProfileSrvFn
        ]);

    function ProfileSrvFn( $resource ) {

        var services = {};

        services.profile = $resource('api/users/:userId',
            { userId : '@_id' },
            {
                get     : { method: 'GET' },
                update  : { method: 'PUT' }
            }
        );

        return services;

    }

})();