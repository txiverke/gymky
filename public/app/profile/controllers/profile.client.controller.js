/**
 * Gymky.me
 * Created by xavi on 15/11/15.
 */

(function() {

    'use strict';
    angular
        .module('app.profile')
        .controller('ProfileController', [
            '$stateParams',
            '$state',
            '$filter',
            '$timeout',
            'Upload',
            'AuthenticationService',
            'ProfileService',
            ProfileCtrlFn
        ]);

    function ProfileCtrlFn($stateParams, $state, $filter, $timeout, Upload, AuthenticationSrv, ProfileSrv ){

        var self = this;
        self.authentication = AuthenticationSrv.getUser();
        self.loading = true;
        self.success = false;
        self.params = $state.params;

        __checkAuthentication();

        self.responseHandlers = {

            getProfile: function() {
                self.loading = false;
                self.success = true;
                $timeout( function(){ $state.go('login') }, 1500 );

            },
            setProfile: function (data){
                self.user = data;
                self.loading = false;
            },
            getError: function( errorResponse ) {
                self.error = errorResponse;
                self.looding = false;
            }

        };

        self.uploadAvatar = function( image ) {

            if ( angular.isArray( image ) ) image = image[0];

            if ( image.type !== 'image/png' && image.type !== 'image/jpeg' ) {
                var errorResponse = $filter('translate')('ADD_ACTIVITY_MSG');
                self.responseHandlers.getError( errorResponse )
            }

            self.loading = true;
            self.uploadInProgress = true;
            self.uploadProgress = 0;

            var userId = self.params.userId;
            var defaultParams = {
                url: 'api/users/' + userId + '/avatar',
                method: 'PUT',
                data: { file : image }
            };

            Upload.upload( defaultParams )
                .success(function( data ){
                    self.responseHandlers.setProfile(data);
                })
                .error(function(err){
                    var error = 'Error uploading file: ' + err.message || err;

                    self.uploadInProgress = false;
                    self.responseHandlers.getError(error)
                }
            );

        };

        self.getProfile = function() {

            var userId = { userId: $stateParams.userId }

            ProfileSrv.profile.get(
                userId,
                self.responseHandlers.setProfile,
                self.responseHandlers.getError
            );

        };

        self.updateProfile = function() {

            self.loading = true;

            var userId = { userId: $stateParams.userId }

            ProfileSrv.profile.update(
                userId,
                self.user,
                self.responseHandlers.getProfile,
                self.responseHandlers.getError
            );

        };

        self.cancelEditProfile = function(){

            $state.go('login');

        };

        /**
         * Check User Authentication
         * @private
         */
        function __checkAuthentication() {

            if (self.authentication.user === null) $state.go('login');

        }

    }


})();
